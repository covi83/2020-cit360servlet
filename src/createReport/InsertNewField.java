package createReport;

import org.hibernate.Session;

import java.util.Scanner;

public class InsertNewField {
    public static void InsertField(){
        System.out.println("Add a new field");

        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();

        Scanner input = new Scanner(System.in);
        System.out.println("Enter new field name: ");
        String FieldName = input.nextLine();

        //Object of the field table to insert new field
        FieldTable newField = new FieldTable();
        newField.setFieldname(FieldName);

        session.save(newField);

        session.getTransaction().commit();
        HibernateUtils.shutdown();
    }
}
