package createReport;

import callMyServlet.ServletClass;

import java.util.List;

public class StartReport {
    public static void main(String[] args) {

        ConnectToDB makeNewSession = ConnectToDB.getInstance();

        List<EmployeeTable> c = makeNewSession.getEmployeesNames();

        for (EmployeeTable i : c) {
            System.out.println(i);
        }

        InsertNewEmployee.InsertEmployee();

        System.out.println("======  Fields  ==========");
        ConnectToDB makeNewField = ConnectToDB.getInstance();
        List<FieldTable> f = makeNewField.getAllfields();
        for (FieldTable i : f){
            System.out.println(i);
        }
        InsertNewField.InsertField();
    }

}
