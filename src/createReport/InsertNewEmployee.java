package createReport;

import org.hibernate.Session;

import java.util.Scanner;

public class InsertNewEmployee {
    public static void InsertEmployee(){
        System.out.println("Add a new employee");

        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();

        Scanner input = new Scanner(System.in);
        System.out.println("Enter employee First name: ");
        String Fname = input.nextLine();
        System.out.println("Enter employees Last name: ");
        String Lname = input.nextLine();

        //Object of the employee table to insert new employee
        EmployeeTable newEmployee = new EmployeeTable();
        newEmployee.setEmployeeFname(Fname);
        newEmployee.setEmployeeLname(Lname);

        session.save(newEmployee);

        session.getTransaction().commit();
//        HibernateUtils.shutdown();
    }
}
