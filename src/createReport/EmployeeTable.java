package createReport;


import javax.persistence.*;

@Entity
@Table(name="employee")
public class EmployeeTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idemployee")
    private int idemployee;

    @Column(name = "employeeFname")
    private String employeeFname;

    @Column(name = "employeeLname")
    private String employeeLname;

    public int getIdemployee() {
        return idemployee;
    }

    public void setIdemployee(int idemployee) {
        this.idemployee = idemployee;
    }

    public String getEmployeeFname() {
        return employeeFname;
    }

    public void setEmployeeFname(String employeeFname) {
        this.employeeFname = employeeFname;
    }

    public String getEmployeeLname() {
        return employeeLname;
    }

    public void setEmployeeLname(String employeeLname) {
        this.employeeLname = employeeLname;
    }
    public String toString(){
        return Integer.toString(idemployee) + " " + employeeFname + " " + employeeLname;
    }
}
