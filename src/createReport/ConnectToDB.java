package createReport;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

import jdk.jfr.events.ExceptionThrownEvent;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
//import org.omg.CORBA.UserException;

import java.util.*;

public class ConnectToDB {
    SessionFactory factory = null;
    Session session = null;

    private static ConnectToDB single_instance =  null;

    private ConnectToDB(){
        factory = HibernateUtils.getSessionFactory();
    }

    public static ConnectToDB getInstance(){
        if (single_instance == null){
            single_instance = new ConnectToDB();
        }return single_instance;
    }

    public List<EmployeeTable> getEmployeesNames(){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            // This line creates the SQL statement to get all the employees in the table.
            String sql = "from createReport.EmployeeTable";
            List<EmployeeTable> cs = (List<EmployeeTable>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
    public EmployeeTable getEmployee(int idemployee){
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from createReport.EmployeeTable where idemployee=" + Integer.toString(idemployee);
            EmployeeTable c = (EmployeeTable)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

    public List<FieldTable> getAllfields(){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from createReport.FieldTable";
            List<FieldTable> ft = (List<FieldTable>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return ft;
        }catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }


}
