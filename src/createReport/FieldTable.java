package createReport;

import javax.persistence.*;

@Entity
@Table(name="fieldname")
public class FieldTable {

    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfield")
    private int idfield;

    @Column(name = "fieldname")
    private String fieldname;

    public int getIdfield() {
        return idfield;
    }

    public void setIdfield(int idfield) {
        this.idfield = idfield;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }
    public String toString(){
        return Integer.toString(idfield) + " " + fieldname;
    }
}
