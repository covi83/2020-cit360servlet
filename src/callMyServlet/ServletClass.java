package callMyServlet;

import createReport.ConnectToDB;
import createReport.EmployeeTable;
import createReport.InsertNewEmployee;
import createReport.StartReport;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "ServletClass", urlPatterns ={"/ServletClass"})
public class ServletClass extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Returns a PrintWriter Object that can return character data to the client (tomcat.apache.org).
        PrintWriter out = response.getWriter();
        // Sets the content type of the response
        response.setContentType("text/html");
        // Initializes the page as HTML
        out.println("<html><head></head><body>");
        // Simple header to notate the beginning of the text posted and returned
        out.println("<p>Here is the text coming from the servlet</p>");
        // Actual value handled through the request and finally the response
        out.println("<p>Servlet Class</p>");
        String employeeName = request.getParameter("textToPost");
        String employeeLastName = request.getParameter("lastNameText");
        out.println("<h1>"+employeeName + " " + employeeLastName+"</h1>");
        // Closes the HTML page
        out.println("</body></html>");




    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
